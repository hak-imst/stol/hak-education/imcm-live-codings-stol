## Containerformate

![Containerformate](images/container_formate.png)

### mp4

Für MPEG-4 Inhalte konzipiert

### AVI

DivX, XviD

### Matroska

MPEG-4 Inhalte und mehr.

### WebM

basiert auf Matroska und wurde speziell zur Nutzung mit dem Website-Standard HTML5 entwickelt. VP8 und VP9 Codec und Vorbis für Audiospuren. 

## Datenkompression

Unterscheide Archivierung (tar) von Komprimierung. ZIP und RAR sind Kombinationen. 

### Verlustlos

Nur möglich, wenn Daten redundant vorliegen. Man kann also eine **Redundanzreduktion** anwenden. Es darf keine Information verloren gehen.

Text lässt sich sehr gut verlustfrei komprimieren:

```
Ausgangstext: AUCH EIN KLEINER BEITRAG IST EIN BEITRAG
Kodiertext: AUCH EIN KLEINER BEITRAG IST /2 /4
```

Tokenbasierte Methode:

```
Ausgangstext: Print "Hallo"; Print "Hier"
Kodiertext: 3F "Hallo"; 3F "Hier"
```

Entropiekodierung:

```
WENN HINTER FLIEGEN FLIEGEN FLIEGEN, FLIEGEN FLIEGEN FLIEGEN NACH.
```

| Text | wird ersetzt
|-- |-- |
| _FLIEGEN | 1 |
| WENN_ | 10 |
| _NACH. | 11 |
| HINTER | 100 |
| , | 101 |



### Verlustbehaftet

Es wird **Threshold** festgelegt welche Daten "wichtig" sind und die "unwichtigen" werden dann verworfen. Damit können alle Daten komprimiert werden.

Beispiel Audio: nicht hörbare Frequenzen können entfernt werden. Stärker durch entfernen "nicht wichtiger". mp3 z.B.

In der optischen Wahrnehmung sind etwa Kanten bedeutender als Farbverläufe. 

![jpeg Komprimierung](images/jpeg_bild.png)

Bei Bewegtbildern werden Einzelbilder komprimiert (JPEG). Höhere Kompressionen konnten durch Untersuchung der Ähnlichkeit "benachbarter" Bilder erreicht werden. 

## Video-Kompressionsformate

### MPEG-4 Standard

Containerformat ist mp4. 

Video Kompressionen die den Standard implementieren: DivX, Xvid, FFmpeg, x264, H.264

Audio Kompressionen: FAAC, AAC